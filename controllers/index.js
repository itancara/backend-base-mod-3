const models = require('../models')

module.exports = () => {
  return {
    UsuarioController: require('./UsuarioController')(models),
    MenuController: require('./MenuController')(models),
    RolController: require('./RolController')(models),
  }
}