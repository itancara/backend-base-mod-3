module.exports = (models) => {
  const { menu } = models;

  function getQuery (options = {}, arr = []) {
    const query = {};
    if (options.limit) {
      query.limit = parseInt(options.limit);
      if (options.page) {
        query.offset = parseInt((options.page - 1) * options.limit);
      }
    }
  
    if (!options.order) {
      options.order = 'createdAt';
    }
  
    if (arr.indexOf(options.order ? options.order.replace('-', '') : null) === -1) {
      if (options.order) {
        if (options.order.startsWith('-')) {
          query.order = [
            [options.order.substring(1), 'DESC']
          ];
        } else {
          query.order = [
            [options.order, 'ASC']
          ];
        }
      }
    }
    query.order.push(['id', 'ASC']);
    return query;
  }

  async function findAll (req, res) {
    const query = getQuery(req.query);

    const menus = await menu.findAndCountAll(query)
    res.status(200).json({
      finalizado: true,
      mensaje: 'OK',
      datos: menus
    })
  }

  async function crear (req, res) {
    const body = req.body;
    const menuCreado = await menu.create(body);
    res.status(201).json({
      finalizado: true,
      mensaje: 'OK',
      datos: menuCreado
    })
  }

  async function actualizar (req, res) {
    const { id } = req.params;
    const body = req.body;
    const menuActualizado = await menu.update(body, { where: { id } });
    res.status(200).json({
      finalizado: true,
      mensaje: 'OK',
      datos: menuActualizado
    })
  }

  async function eliminar (req, res) {
    const { id } = req.params;
    const menuEliminado = await menu.destroy({ where: { id } });
    res.status(200).json({
      finalizado: true,
      mensaje: 'OK',
      datos: menuEliminado
    })
  }


  return {
    findAll,
    crear,
    actualizar,
    eliminar
  }
}