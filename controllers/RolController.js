module.exports = (models) => {
  const { rol, rol_menu, menu } = models;

  function getQuery (options = {}, arr = []) {
    const query = {};
    if (options.limit) {
      query.limit = parseInt(options.limit);
      if (options.page) {
        query.offset = parseInt((options.page - 1) * options.limit);
      }
    }
  
    if (!options.order) {
      options.order = 'createdAt';
    }
  
    if (arr.indexOf(options.order ? options.order.replace('-', '') : null) === -1) {
      if (options.order) {
        if (options.order.startsWith('-')) {
          query.order = [
            [options.order.substring(1), 'DESC']
          ];
        } else {
          query.order = [
            [options.order, 'ASC']
          ];
        }
      }
    }
    query.order.push(['id', 'ASC']);
    return query;
  }

  async function findAll (req, res) {
    const query = getQuery(req.query);
   
    const roles = await rol.findAndCountAll(query)
    res.status(200).json({
      finalizado: true,
      mensaje: 'OK',
      datos: roles
    })
  }

  async function crear (req, res) {
    const body = req.body;
    const rolCreado = await rol.create(body);
    if (body.menus) {
      for (const menu of body.menus) {
        await rol_menu.create({ id_rol: rolCreado.dataValues.id, id_menu: menu });
      }
    }
    res.status(201).json({
      finalizado: true,
      mensaje: 'OK',
      datos: rolCreado
    })
  }

  async function actualizar (req, res) {
    const { id } = req.params;
    const body = req.body;
    const rolActualizado = await rol.update(body, { where: { id } });

    if (body.menus) {
      await rol_menu.destroy({ where: { id_rol: id } })
      for (const menu of body.menus) {
        await rol_menu.create({ id_rol: id, id_menu: menu });
      }
    }

    res.status(200).json({
      finalizado: true,
      mensaje: 'OK',
      datos: rolActualizado
    })
  }

  async function eliminar (req, res) {
    const { id } = req.params;
    const rolEliminado = await rol.destroy({ where: { id } });
    res.status(200).json({
      finalizado: true,
      mensaje: 'OK',
      datos: rolEliminado
    })
  }


  return {
    findAll,
    crear,
    actualizar,
    eliminar
  }
}