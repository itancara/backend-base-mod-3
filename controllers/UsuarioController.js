module.exports = (models) => {
  const { usuario } = models;

  function getQuery (options = {}, arr = []) {
    const query = {};
    if (options.limit) {
      query.limit = parseInt(options.limit);
      if (options.page) {
        query.offset = parseInt((options.page - 1) * options.limit);
      }
    }
  
    if (!options.order) {
      options.order = 'createdAt';
    }
  
    if (arr.indexOf(options.order ? options.order.replace('-', '') : null) === -1) {
      if (options.order) {
        if (options.order.startsWith('-')) {
          query.order = [
            [options.order.substring(1), 'DESC']
          ];
        } else {
          query.order = [
            [options.order, 'ASC']
          ];
        }
      }
    }
    query.order.push(['id', 'ASC']);
    return query;
  }

  async function login (req, res) {
    res.status(200).json({
      finalizado: true,
      mensaje: 'OK',
      datos: {
        usuario: 'Admin',
        rol: {
          nombre: 'SUPER ADMIN',
          descripcion: 'Super administrador del sistema.'
        },
        menu: [
          {
            url: 'persona',
            label: 'Persona',
            icono: 'People'
          },
          {
            url: 'rol',
            label: 'Roles',
            icono: 'group'
          },
          {
            url: 'parametro',
            label: 'Parametros',
            icono: 'settings'
          }
        ]
      }
    })
  }

  async function findAll (req, res) {
    const query = getQuery(req.query);

    const usuarios = await usuario.findAndCountAll(query)
    res.status(200).json({
      finalizado: true,
      mensaje: 'OK',
      datos: usuarios
    })
  }

  async function crear (req, res) {
    const body = req.body;
    const usuarioCreada = await usuario.create(body);
    res.status(201).json({
      finalizado: true,
      mensaje: 'OK',
      datos: usuarioCreada
    })
  }

  async function actualizar (req, res) {
    const { id } = req.params;
    const body = req.body;
    const usuarioActualizado = await usuario.update(body, { where: { id } });
    res.status(200).json({
      finalizado: true,
      mensaje: 'OK',
      datos: usuarioActualizado
    })
  }

  async function eliminar (req, res) {
    const { id } = req.params;
    const usuarioEliminado = await usuario.destroy({ where: { id } });
    res.status(200).json({
      finalizado: true,
      mensaje: 'OK',
      datos: usuarioEliminado
    })
  }


  return {
    findAll,
    crear,
    actualizar,
    eliminar,
    login
  }
}