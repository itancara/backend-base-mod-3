'use strict';
module.exports = (sequelize, DataTypes) => {
  const usuario = sequelize.define('usuario', {
    nombres: DataTypes.STRING,
    apellidoPaterno: DataTypes.STRING,
    apellidoMaterno: DataTypes.STRING,
    id_rol: DataTypes.INTEGER
  }, {
    tableName: 'usuario'
  });
  usuario.associate = function(models) {
  };
  return usuario;
};