'use strict';
module.exports = (sequelize, DataTypes) => {
  const rolMenu = sequelize.define('rol_menu', {
    id_rol: DataTypes.INTEGER,
    id_menu: DataTypes.INTEGER,
  }, {
    tableName: 'rol_menu'
  });
  rolMenu.associate = function(models) {
  };
  return rolMenu;
};