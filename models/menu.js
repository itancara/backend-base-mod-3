'use strict';
module.exports = (sequelize, DataTypes) => {
  const menu = sequelize.define('menu', {
    nombre: DataTypes.STRING,
    ruta: DataTypes.STRING,
    icono: DataTypes.STRING
  }, {
    tableName: 'menu'
  });
  menu.associate = function(models) {
    // associations can be defined here
    const { rol, usuario, menu, rol_menu } = models
  };
  return menu;
};