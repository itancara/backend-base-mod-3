'use strict';
module.exports = (sequelize, DataTypes) => {
  const rol = sequelize.define('rol', {
    nombres: DataTypes.STRING,
    descripcion: DataTypes.STRING
  }, {
    tableName: 'rol'
  });
  rol.associate = function(models) {

  };
  return rol;
};