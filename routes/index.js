const controllers = require('../controllers')()

module.exports = (app) => {
  return {
    UsuarioRoute: require('./UsuarioRoute')(app, controllers),
    RolRoute: require('./RolRoute')(app, controllers),
    MenuRoute: require('./MenuRoute')(app, controllers),
  }
}