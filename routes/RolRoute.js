
module.exports = (app, controllers) => {
  const { RolController } = controllers;

  app.get('/rol', RolController.findAll)
  app.post('/rol', RolController.crear)
  app.put('/rol/:id', RolController.actualizar)
  app.delete('/rol/:id', RolController.eliminar)
  
}
