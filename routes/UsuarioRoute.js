
module.exports = (app, controllers) => {
  const { UsuarioController } = controllers;

  app.get('/usuario', UsuarioController.findAll)
  app.post('/login', UsuarioController.login)
  app.post('/usuario', UsuarioController.crear)
  app.put('/usuario/:id', UsuarioController.actualizar)
  app.delete('/usuario/:id', UsuarioController.eliminar)
  
}
