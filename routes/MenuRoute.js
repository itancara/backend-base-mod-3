
module.exports = (app, controllers) => {
  const { MenuController } = controllers;

  app.get('/menu', MenuController.findAll)
  app.post('/menu', MenuController.crear)
  app.put('/menu/:id', MenuController.actualizar)
  app.delete('/menu/:id', MenuController.eliminar)
  
}
